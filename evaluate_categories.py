#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np

import util

# slightly increase font size for plots
import matplotlib
matplotlib.rcParams.update({'font.size': 13})

def parseCategories(categoryFile):
    """
    Parse a category file into a dictionary. The dict will contain
    each category as a key. The value of the category is a list whose
    first element is the number of problems/improvements in this category.
    The second element is another dictionary mapping each subcategory
    to the number of problems/improvements in it.

    :param categoryFile: the file containing categorized problems/improvements
    :return: a dictionary for the categories as described above
    """
    # save identifier (tuple containing participants id and answer placement) to print
    # if there are problems/improvements in more than one category
    identifiers = []
    # create an empty dict
    categories = {}
    # open the file
    with open(categoryFile, 'r') as f:
        # create values saving the current category and subcategory
        category = None
        subcategory = None
        # iterate over lines in the file
        for line in f:
            # subcategories are sub-headers in markdown and thus start with to hash signs
            if line.startswith('##'):
                # remove the has signs, strip the category name and add it to the dict
                subcategory = line.replace('##', '').strip()
                categories[category][1][subcategory] = 0
                continue
            # categories are headers in markdown and thus start with one hash sign
            if line.startswith('#'):
                # remove the hash sign, strip the category name and add it to the dict 
                category = line.replace('#', '').strip()
                categories[category] = [0, {}] 
                # clear the subcategory because we are now in a new category
                subcategory = None
                continue
            # elements are unordered items in markdown and thus start with a star
            if line.startswith('*'):
                # extract identifier tuple
                identifier = line.split(']')[0]
                identifier = identifier[3:len(identifier)]
                split = identifier.split(':')
                identifier = ((int(split[0]), int(split[1])))
                # write information if it is mentioned more than once
                if identifier in identifiers:
                    print(identifier, ' is in more than one category!')
                else:
                    identifiers.append((int(split[0]), int(split[1])))

                # count up the number of elements in a category
                # categories[category][0] += 1
                # if we have a subcategory also update its count
                if subcategory is not None:
                    categories[category][1][subcategory] += 1
                else:
                    categories[category][0] += 1

                continue
            # every other type of line are interpreted as comments and not counted by this script

        # if a category has subcategories but they do not contain all the elements
        # of the category, add the category itself as a subcategory with the remaining elements
        for category in categories:
            subcategories = categories[category][1]
            # continue if there are no subCategories
            if len(subcategories) == 0:
                continue

            # verify that the subcategories do not account for all elements in the category
            if sum(subcategories.values()) < categories[category][0]:
                categories[category][1][category] = categories[category][0] - sum(subcategories.values()) 
    return categories


def printCategories(categories):
    """
    Print the categories in a nice fashion.

    :param categories: a dictionary of categories as created by the parsing method.
    """
    categoryLengthFormatter = util.getLengthFormatter(categories.keys())
    for category in categories:
        print(' - ' + categoryLengthFormatter.format(category) + ' :', categories[category][0])
        subcategories = categories[category][1]
        if len(subcategories) == 0:
            continue

        subcategoryLengthFormatter = util.getLengthFormatter(subcategories.keys())
        for subcategory in subcategories:
            print('    - ' + subcategoryLengthFormatter.format(subcategory) + ' :', subcategories[subcategory])
        
def createCSV(categories, t, header=True):
    """
    Create a csv file representation for the categorization.
    The result contains four columns titled: Type, Category, Subcategory and Amount.

    :param categories: the dictionary of categories.
    :param t: the Type added for all rows.
    :param header: if the header containing the names of the columns should be added.
    :return: a string which can be written to a csv file.
    """
    csv = ''
    if header:
        csv = 'Type,Category,Subcategory,Amount\n'

    for category in categories:
        csv += t + ',' + category + ',' + category + ',' + str(categories[category][0]) + '\n'

        subcategories = categories[category][1]
        for subcategory in subcategories:
            csv += t + ',' + category + ',' + subcategory + ',' + str(subcategories[subcategory]) + '\n'

    return csv


def createBarPlot(categories, withSubCategories=True):
    """
    Create a bar plot for categories and subcategories.

    :param categories: a dictionary of categories as created by the parsing method.
    :param withSubCategories: boolean to indicate whether to draw bars for subcategories on top of category bar or not.
    :return: the created figure.
    """
    # save keys as list to allow ordering
    categoryList = categories.keys()
    # order that largest category is at the front
    categoryList.sort(key=(lambda x: categories[x][0]), reverse=True)

    # create figure with size depending on the number of categories
    fig, ax = plt.subplots(figsize=(len(categoryList) * 2, 7))
    # this is the default barwidth - save to compute values for subcategory plot
    barWidth = 0.8
    # create bar plot for categoris and add category names as labels
    rect = ax.bar(np.arange(len(categoryList)), map(lambda x: categories[x][0], categoryList), tick_label=map(lambda x: util.insertLineBreaks(x, limit=8), categoryList), width=barWidth)
    # write size of bar above each
    util.autolabelBarchart(rect, ax)

    # if no sub Category plot required return
    if not withSubCategories:
        return fig

    # iterate over subcategories
    for i in range(len(categoryList)):
        # save sub category dict
        subcategories = categories[categoryList[i]][1]
        # skip if there are no subcategories
        if len(subcategories) == 0:
            continue

        # save category list to allow sorting
        subcategoryList = subcategories.keys()
        # sort so that largest subcategory is at the front
        subcategoryList.sort(key=(lambda x: subcategories[x]), reverse=True)

        # compute interval into which the bars for the sub categories need to be placed
        # to be inside the bar of the category
        start = i - 0.5 * barWidth
        end = i + 0.5 * barWidth
        # compute width of sub category bars
        width = float(end - start) / len(subcategoryList)
        # plot bars, interval has to start in the middle of the first bar so move start 0.5 width to the right
        # make width slightly smaller to have some empty space between bars
        # set color to be visually different from main bars and even recognizable in black and white
        rect = ax.bar(np.arange(start + 0.5 * width, end, width), map(lambda x: subcategories[x], subcategoryList), width=0.8 * width, color='lightgreen')
        # write size of bar above each
        util.autolabelBarchart(rect, ax)
    return fig

# parse categories, print info and save plot
problemCatgories = parseCategories('categorized_problems.md')
print('Problems:')
printCategories(problemCatgories)
del problemCatgories['Unknown']
# fig = createBarPlot(problemCatgories)
# util.saveFigure(fig, 'categorization_problems')

print('')

# parse improvements, print info and save plot
improvementCategories = parseCategories('categorized_improvements.md')
print('Improvements:')
printCategories(improvementCategories)
del improvementCategories['Unknown']
# fig = createBarPlot(improvementCategories)
# util.saveFigure(fig, 'categorization_improvements')

csv = createCSV(problemCatgories, 'Problems')
csv += createCSV(improvementCategories, 'Improvements', header=False)
f = open('categorization.csv', 'w')
f.write(csv)
f.close()

# reactivate to show plot
# plt.show()


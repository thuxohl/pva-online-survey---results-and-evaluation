#!/usr/bin/env python3

"""
Script containing utility methods such as loading the survey results
into a list and filtering participants that did not answer the control
question correctly.
"""

import csv

"""
Indices for control question.
Extracted by invoking this script.
"""
smartSpeakerQuestion1 = 43
smartSpeakerQuestion2 = 52


def stringToInt(string):
    """
    Convert a string in the data to an integer. String in the data
    are saved as floats. Therefore, this method first converts the string
    to a float and then to an int.
    In addition, empty string are mapped to 0 to allow comparisions of the
    control question (the first question did not have to be answered which
    is why participants without smart speaker probably did not provid an
    answer resulting in an empty string).

    :param string: the string to be converted.
    :return: the resulting integer as described above.
    """
    if string == '':
        return 0;

    return int(float(string))


def loadSurveyResults(printInfo=False):
    """
    Load the survey results csv data and parse them into a list where each
    element contains a list with all answers from a participant.
    This method automatically filters participants that did not answer the control
    question correctly.

    :param printInfo: if True data is not loaded but all questions with their
                      indices are printed.
    :return: a list where each element contains a list of all answer fo a participant.
             For indices to that list invoke this method with printInfo=True.
    """
    filename = 'survey_results.csv'
    rowLength = -1
    rows = []
    with open(filename, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='"')
        skipFirst = True
        for row in reader:
            # last row is always empty so stop then
            if len(row) == 0:
                break
            # filter the first row wich are the question codes
            if skipFirst:
                if printInfo:
                    for i in range(len(row)):
                       print(i, row[i])
                    return None
                rowLength = len(row)
                skipFirst = False
                continue
            if len(row) != rowLength:
                print('Error in participant-row', row[0], 'has length', len(row), 'but should be', rowLength)
            # filter rows from users that did not answer the control question correctly
            answer1 = stringToInt(row[smartSpeakerQuestion1])
            answer2 = stringToInt(row[smartSpeakerQuestion2])
            if answer1 != answer2:
                # print 'Filter participant', row[0], 'because control question answers differ', answer1, '!=', answer2
                continue
            # add row
            rows.append(row)
        # return complete list
        return rows


def yesOrNoToNumber(y):
    """
    Convert a yes or no answer to the number 1 or 0.

    :param y: the yes or no answer as a string.
    :return: 1 if the answer is 'yes', 0 if it is 'no' and None if it does not macth yes or no.
    """
    if y == 'Yes':
        return 1
    elif y == 'No':
        return 0
    return None 


def expToNumber(exp):
    """
    Convert a string representing an experience rating to a number.
    This ranges from 1 (not experienced at all) to 5 (extremely experienced).

    :param exp: a string representing the experience rating.
    :return: a number from 1 to 5 or None if the string does not match any rating.
    """
    if exp.startswith('not'):
        return 1
    elif exp.startswith('slightly'):
        return 2
    elif exp.startswith('moderately'):
        return 3
    elif exp.startswith('very'):
        return 4
    elif exp.startswith('extremely'):
        return 5
    return None


def saveFigure(fig, name):
    """
    Save a figure created by pyplot as a pdf with a tight bounding box.
    The figure is saved to the figures directory.

    :param fig: the figure to be saved.
    :param name: the name given the figure. The file extension '.pdf' will be applied to this.
    """
    fig.savefig('figures/' + name + '.pdf', bbox_inches="tight")


def thresholdDict(dictToTreshold, threshold, otherKey='Other'):
    """
    Apply a threshold to a dictionary which maps some keys to integers.
    A new dictionary will be returned in which every key with a value below
    the threshold is mapped to the other key.

    :param dictToTreshold: a dictionary mapping some keys to integers.
    :param threshold: the threshold to be applied.
    :param otherKey: the key as which values below the threshold are summarized.
    :return: a new dictionary with the threshold applied as described.
    """
    newDict = { otherKey: 0 }
    for key in dictToTreshold:
        if dictToTreshold[key] < threshold:
            newDict[otherKey] += dictToTreshold[key]
        else:
            newDict[key] = dictToTreshold[key]
    return newDict


def insertLineBreaks(string, limit=15):
    """
    Insert line breaks into a string at spaces. A line break
    is inserted if the next word plus a space exceeds the provided limit.
    
    :param string: the string into which line breaks are inserted.
    :param limit: number of charactes which cause a line break if exceeded.
    :return: a string with line breaks inserted
    """
    split = string.split(' ')
    if len(split) == 0:
        return ''
    result = split[0]
    currentLength = len(result)
    for i in range(1, len(split)):
        value = split[i]
        if currentLength + len(value) + 1 > limit:
            currentLength = len(value)
            result += '\n' + value
        else:
            currentLength += 1 + len(value)
            result += ' ' + value
    return result


def autolabelBarchart(rects, ax, xpos='center'):
    """
    Attach a text label above each bar in *rects*, displaying its height.

    *xpos* indicates which side to place the text w.r.t. the center of
    the bar. It can be one of the following {'center', 'right', 'left'}.
    """
    xpos = xpos.lower()  # normalize the case of the parameter
    ha = {'center': 'center', 'right': 'left', 'left': 'right'}
    offset = {'center': 0.5, 'right': 0.57, 'left': 0.43}  # x_txt = x + w*off

    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()*offset[xpos], height, '{}'.format(height), ha=ha[xpos], va='bottom')


def getLengthFormatter(stringList):
    maxLength = max(map(lambda x: len(x), stringList))
    return '{:<' + str(maxLength) + '}'


# When exeucted the util script prints all questions with the indices
# at which they can be accessed.
if __name__ == '__main__':
    loadSurveyResults(printInfo=True)

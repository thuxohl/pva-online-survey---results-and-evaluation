#!/usr/bin/env python3

"""
Script evaluating the demographic questions of the survey by
printing information about the answers.
"""

import numpy as np
import matplotlib.pyplot as plt

import util

"""
Indices for demographic questions.
These were extracted by reading the output of the util script.
"""
ageIndex = 6
genderIndex = 7
occupationIndex = 8
countryOfResidenceIndex = 9

# Create map to store values
values = { \
        ageIndex: [], \
        genderIndex: {}, \
        occupationIndex: {}, \
        countryOfResidenceIndex: {} \
      }


# Create function which maps some answers to occupation to a common name
def mapOccupation(occupation):
    if occupation.lower().startswith('it') or ' it ' in occupation.lower():
        return 'IT'
    if 'software' in occupation.lower():
        return'IT'
    if 'computer' in occupation.lower():
        return 'IT'
    if 'developer' in occupation.lower() or 'devleoper' in occupation.lower():
        return 'IT'
    if 'system admin' in occupation.lower() or 'network admin' in occupation.lower():
        return 'IT'
    if occupation.lower() == 'student' or occupation.lower() == 'studentin':
        return 'Student'
    if occupation == 'Wissenschaftler' or occupation == 'Physiker':
        return 'Scientist'
    if 'unemployed' in occupation.lower():
        return 'Unemployed'
    return occupation;


# Create function which maps some answers to country of residence to a common name
def mapCountryOfResidence(countryOfResidence):
    if 'deutschland' in countryOfResidence.lower():
        return 'Germany'
    if 'usa' in countryOfResidence.lower() or countryOfResidence.lower() == 'us':
        return 'USA'
    if 'united states' in countryOfResidence.lower():
        return 'USA'
    if countryOfResidence.lower() == 'america':
        return 'USA'
    return countryOfResidence


# iterate over data and fill value map
for row in util.loadSurveyResults():
    # values for age are stored as string representations of float
    # therefore parse as float and then cast to int
    values[ageIndex].append(int(float(row[ageIndex])))
    # increase the number how many times a gender was mentioned
    values[genderIndex][row[genderIndex]] = values[genderIndex].get(row[genderIndex], 0) + 1
    # map occupations to a common name
    occupation = mapOccupation(row[occupationIndex])
    # increase the number how many times an ocuupation was mentioned
    values[occupationIndex][occupation] = values[occupationIndex].get(occupation, 0) + 1
    # map country of residence to a common name
    countryOfResidence = mapCountryOfResidence(row[countryOfResidenceIndex])
    # increase the number how many times a country was mentioned
    values[countryOfResidenceIndex][countryOfResidence] = values[countryOfResidenceIndex].get(countryOfResidence, 0) + 1

# threshold occupations so that everything mentioned only once is mapped to other
values[occupationIndex] = util.thresholdDict(values[occupationIndex], 2)
# threshold countries so that everything mentioned less than three times is mapped to other
values[countryOfResidenceIndex] = util.thresholdDict(values[countryOfResidenceIndex], 3)

# print information
numberFormat = '{:.1f}'
print('Age:')
print(' - μ = ' + numberFormat.format(np.mean(values[ageIndex])) + ', σ = ' + numberFormat.format(np.std(values[ageIndex])) + ', Max = ' + str(max(values[ageIndex])) + ', Min =' + str(min(values[ageIndex])))
print('')
print('Gender:')
for gender in values[genderIndex]:
    print(' - ' + gender + ':', values[genderIndex][gender])
print('')
print('Occupation:')
for occupation in values[occupationIndex]:
    print(' - ' + occupation + ':', values[occupationIndex][occupation])
print('')
print('Country of Residence:')
for countryOfResidence in values[countryOfResidenceIndex]:
    print(' - ' + countryOfResidence + ':', values[countryOfResidenceIndex][countryOfResidence] )

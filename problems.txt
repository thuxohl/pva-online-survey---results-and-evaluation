[1:0] Google Home versteht mich falsch (insbesondere wenn ich englischsprachige Musik abspielen möchte) oder springt gar nicht erst an.
[1:1] Wenn ich einen Film mit Sprachsteuerung pausiere und dann 'Weiter' statt 'Fortsetzen' sage, wechselt er zur nächsten Folge der Serie.
[5:0] Verbindungsabbruch
[5:1] Fühlt sich angesprochen obwohl nicht gemeint
[9:0] Sprachkommandos werden nicht richtig verarbeitet. Licht kann im Schlafzimmer gedimmt werden. Im Büro steuert er allerdings alle Lampen in der ganzen Wohnung.
[9:1] Farbkommandos werden teilweise nicht erkannt für bestimmte Lampen. Dies könnte an den etwas kompliziert gewählten Lampennamen liegen. (In diesem Fall "Steh1" und "Steh 2").
[9:2] Mehr Geräte einbinden, die sich steuern lassen.
[9:3] Einfachere Vernetzung vorhandener Hardware, welche z.B. Bluetooth besitzen in das aktuelle Smarthome.
[11:0] Verstehe nur klare Anweisungen.
[11:1] Sie reagieren bloß, dadurch kommen keine echten Gespräche auf. Ich würde einem echten Menschen ja auch nicht mit den Worten „Stelle einen Wecker auf...“ begegnen, sondern in höflich darum bitten.
[12:0] Bei benutzen von Alltagssprache wird die Intention des Befehls manchmal nicht verstanden / falsch interpretiert. (Vs. benutzen von eindeutigen Anweisungen)
[15:0] Voice recognition. I use a Google home with the voice training activated. Sometimes when I ask it if I have any reminders it says it can't recognize me. Takes a few tries for it to work.
[15:1] Can't send texts through the Google Home
[17:0] Spotty voice recognition Too frequently it is unable to answer random questions
[17:1] Larger set of answerable question types
[17:2] Plays wrong music playlists (usually with similar but not identical names)
[17:3] Can't "broadcast" to just one other speaker
[19:0] Amazon's Alexa range will not work without an internet connection. This makes sense for the most part, but it would be nice to have a portion of her abilities available offline, such as interacting with my smart home hub, telling the time, etc.
[19:1] Alexa seems to struggle hearing people when there is background noise or other people talking.
[20:0] Having to say ok google everytime i want something, sometimes few times in the same proces
[20:1] google cant turn off lights in the room its standing, you cant just say turn off lights, because it will turn them off in whole house, you have to specify the room
[21:0] The fact that Google assistant  can't keep a conversation running with you. Example: you ask him something then want to interact with it about the anwser. "I didn't understood your question"
[21:1] Sometimes Google Assistant give you too much information. When you say "Hello" it's giving Weather and Radio News
[24:0] Gefahr der "Spionage"/Abhörung durch die Geräte.
[26:0] Gets caught up in our regular conversation.
[26:1] Smartphone app- SmartThings does not work , or is very long delay.
[29:0] Mis understanding me
[29:1] Picking up without the key words
[31:0] Interpretability
[31:2] Smart device doesn't understand your question
[33:0] Not understanding what I've said. I don't have an accent and I speak clear, fluent english.
[33:1] Answering more complex questions. Currently, the Echo is only able to answer questions that are easily searched on Bing. This is fine for simple questions but if you ask it more complex questions you get "Hmm, I'm not sure how to help you with that."
[37:0] Losing connection to cloud services (due to temporary connectivity issues or the business closing)
[43:0] Siri does not understand multiple commands in one sentencte, for example: turn on the lights in the kitchen at 100%, and activate Relaxing scene in the living room, and set the temperature to 21 degrees.
[48:0] Lackluster voice recognition.  I'll issue one command, and Alexa will either not recognize it or interpret a different command.
[48:1] No persistent / conversational memory.  After a successful interaction, subsequent interactions have no memory of the previous interaction, as such, natural language pronouns can't be used. "Alexa, when is my next appointment?" "Tuesday, at 11am" "Alexa, cancel it" "I can't do that."
[48:2] Nope.
[50:0] Not understanding what I wanted.
[50:1] Device does not turn on/off because of connectivity
[53:0] My torrent box sometimes gets stuck (RPi3 B+ bug) and I have to restart it.
[53:1] The cats spread litter all around the litter boxes.
[54:0] They take over voice control from my phone, without the same capabilities. “Launch the ... app”.
[54:1] They have a problem with volume being too loud when responding to something. As well as responding to requests that don’t need them, such as turning devices on.
[54:2] Generic disconnections of devices.
[58:0] schlechte Erkennung des Hotwords/Spracherkennung
[58:1] wenig Flexibilität des Systems bei abgewandelten Befehlen, anderen Bedürfnissen (zB schlalte das Licht in 5 Minuten aus)
[59:0] I don’t currently use voice control for smart homes
[59:2] Smart homes don’t remember my routines. I want a smart home that learns about me and proactively works to support me. Example - my alarm is set for 6:30 AM, but my coffee pot has to be set separately. I want my coffee pot to know and turn on at the same time.
[59:3] Voice control is only one way - I provide the input. I want voice notification to identify who and what in real time.
[61:0] Lack of context awareness
[61:1] Systems are set up for use cases, they do not learn our habits.E.g. I walk into bedroom before sleep, light turns on/stays on for 5 mins and turns off. When i do the same mid-day, it needs to stay on. I shouldn't be programming this.
[61:2] Nothing new, same as before.
[61:3] Nothing new. Same as before
[64:0] Limited capabilities/app integration.
[64:1] Lack of understanding of basic commands.
[71:0] There are too many iot communication standards. Not one unifying service that can control everything
[71:1] Older technology that doesn't work with smart home devices
[78:0] sometimes confuses two for too (when controlling multiple similar devices with sequential names)
[78:1] with alexa, it being WAY to particular on command phraseology
[78:2] not always 'waking up' when commanded
[81:0] No learning functionality from Alexa. She doesn’t know what I like or learn about me.
[81:1] Alexa skills are not good.
[81:2] No additional problems.
[81:3] No additional problems.
[82:0] Missunderstood command
[82:1] A long delay between when I ask for something and the action is performed
[82:2] The speaker playing a sound can not hear new commands
[83:0] Misheard phrases.
[83:1] Loss of connection
[88:0] Name zur Ansprache inferiert mit anderem Audio: TV-Report löst Aktion im Wohnzimmer aus
[88:1] fehlenes Feedback bzw. nicht unterbrechbares Feedback: System antwortet aber Antwort dauert zu lange, stört und lässt sich nicht sofort unterbrechen... - z.B. System erklärt sich selbst (1min), aber man will bereits nach 5s stoppen und mit anderen Gästen reden
[88:2] Stimme es Systems ist zu leise, unverständlich, merkt nicht, dass ein Störgeräusch ein wichtiges Wort der Systemäußerung übertönt hat...
[88:3] System versteht Anfrage falsch, z.B. schreibt das Wort 'Punkt' anstatt des Satzpunktendzeichens '.'. Nervig
[89:0] Tedious start phrases and the inability to change those phrases. It is sometimes difficult to have it hear you properly the first time.
[89:1] Voicing the results of a command when there is already a visual component (ie,. lights turning on and off)
[92:0] Privacy! These devices are recording your audio in your home and uploading it to non-local "cloud" storage. This is especially problematic when an "over-eager" hotword-detection algorithm triggers too often and records random segments of conversations.
[92:1] Incorrect interpretation of commands.
[95:0] Didn't respond to wake up 1st time (hey Google)
[95:1] Not interpretting my request
[97:0] too limited in capabilities
[100:0] Difficulty understanding kid voices
[103:0] Hard to active speaker while music is playing
[104:0] Features not being consistent world wide
[104:1] Not enough customized commands
[108:0] Failure to recognize activation keywords such as "ok, google..."
[108:1] Failure to communicate with connected devices
[110:0] Incorrect response from device.
[110:1] Lack of native support for some devices.
[114:0] Trigger phrase not being heard
[114:1] "Don't know how to do that"
[117:0] Lack of context.
[117:1] Not enough customization
[120:0] They don't understand your question.
[120:1] Accidently thinks you were talking to it.
[121:0] Requiring one to speak loudly to activate the device with a voice queue.
[121:1] Lack of fluent language switching on-the-go.
[122:0] Unrecognizable request.
[122:1] Missed commands due to cloud.
[124:0] I do not have a good overview of the capabilities of the smart spearker. And I never know if I didn't use the right words or if a functionnality is not supported.
[124:1] Sometimes, from one day to another, a capability disapear. Like if the server working for the functionnality died somewhere...
[127:0] Misunderstands user command
[127:1] "I can't help with that yet" for tasks that seem common, such as changing an alarm time.
[128:0] Clunky
[129:0] Inconsistent automation. Something like turning on the lights when I get home goes through 3 services and 4 pieces of hardware.
[129:1] Difficulty of installation and troubleshooting. Setting up even the most mature of smart home light brands (philips hue) is a pain in the ass and not something my mom could do. When something breaks she has no chance of fixing it.
[133:0] Mis-heard commands or communications
[133:1] Picking up background noise, unintentional phrases,or malicious intentional phrases, as actual commands SEE: South Park
[137:0] Hört nicht auf okay Google
[139:0] comprehension
[139:1] failure to hear commands
[142:0] Report item is offline after turning the item off or on.
[143:0] Keine Internetverbindung --> keine Sprachsteuerung möglich.
[143:1] Verständigungsprobleme
[143:2] Fremde Benutzer könnten an persönliche Daten wie z.B. Kalender-oder Telefonbucheinträge gelangen.
[143:3] Google spricht nachts teils zu laut und könnte dadurch evtl. schlafende Personen in den Nebenräumen aufwecken.

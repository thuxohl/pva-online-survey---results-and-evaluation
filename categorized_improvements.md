# Language Processing

## General

* [12:0] Verbesserte Spracherkennung bei Alltagssprache (Abkürzungen zb.) und Dialekten
* [17:1] Improve voice understanding
* [20:1] adding more languages like polish for example
* [54:0] “Hey ... that’s not what I said” “I’m sorry, please type on your device what you said, and I will attempt to understand you better in the future ”
* [108:0] Better speech recognition training
* [121:0] Enable instant dual-language function.
* [121:1] Improve quality and fluidity of AI voice, as well as the ability to accurately receive the speaker's.

## Command Execution

* [9:1] Mehr Kommandos für ein und die selbe Funktion.
* [29:1] More options, more intuitive control
* [58:0] Verbesserung der Spracherkennung und Interpretation von Befehlen
* [139:0] comprehension

## Hotword

* [78:1] more reliable wakeup phrases or renaming of device to respond to

## Dialogue Context

* [5:0] Folge fragen erkennen
* [31:0] More natural triggers
* [31:1] More natural voice
* [48:0] Pronouns and natural language memory.  See previous comment.

# Capabilities

## Software

* [9:2] Höfliches Sprechen auf irgend eine art belohnen. ;)
* [9:3] Programme einbinden, um Sprachen via Smarthome zu lernen. Für Vokabeln aber ggf. auch für Gramatik. :)
* [11:0] Der Assitent sollte in der Lage sein mir leidige Aufgaben abzunehmen. Züge buchen, einfache Ethernet Aufgaben, ich möchte wissen, was ich im Kühlschrank habe, was kann ich damit kochen?
* [17:2] Add an intercom or walkie talkie capability
* [17:3] Let playing music switch to a different speaker without having to restart the playlist
* [20:0] adding more visual control when you have chromecast with google home
* [24:0] Mehr Fuktionen.
* [26:0] Make Siri work with SmartThings.
* [26:1] Improve control of Home entertainment with harmony
* [29:0] Better intergation with other apps
* [31:2] Device looks at your schedule and weather. Tells you if you need a raincoat, umbrella etc.
* [31:3] Looks at your commute to work data (from smart phone?) and assesses if there's a better/cheaper/faster route
* [39:0] Pausing music on another speaker
* [39:1] Setting an alarm on another speaker
* [59:0] Better integration with alternate smart home apps
* [59:1] More logic integrated into the actions
* [71:1] Communication with other smart assistants
* [81:1] Make automating smart speakers to provide unprovoked information an option. For example, if I arrive home, my smart speaker should know that and begin providing me with my daily update.
* [95:2] Inter-room voice chatting
* [143:1] Nennung der Raumtemperatur.

## Hardware

* [19:0] More devices need to have functionality built in. For example, I currently use smart plugs to give me voice control over fans. This seems to be happening, with more products being released with smart functionality, such as washing machines and coffee makers.
* [21:0] Give us more actuators, to interact with builtin light switches or AC plugs. To make older home smart too
* [33:0] Add-on remote microphones. These little things could be placed throughout the house and connect up to one or many main units. So regardless where I am in the house it would be able to hear me.
* [61:0] Better hardware support. Not everything has to be a speaker, can be remote microphones with central speaker.
* [82:1] A secondary microphone away from the speaker so commands can be heard from multiple parts in the room
* [89:0] Wired options. Most new bleeding each tech is wholly dependant on wireless connections. This is less secure and in crowded residential areas less dependable due to interference on crowed wireless channels. If I have the option I will spend the extra time to wire a device over using wireless if its an option.
* [117:0] Distributed speakers, central processing
* [139:1] better microphones

# User Context

* [9:0] Verschiedene Stimmen erkenne können.
* [81:0] Learn who is speaking and tailor responses for them. If my wife asks “how long is my commute” the smart speaker should respond with her commute, not mine.
* [108:2] Ability to recognize the layout of one's home, based on motion detectors or some sort of access to building plan in order to make assumptions as to what task you are performing based on where in the house you are, and adjust accordingly.
* [108:3] Ability to recognize voices of different users, and respond accordingly
* [129:0] Location aware contexts. If I say turn off the lights it should know where I am in the house and turn off the lights in only that room.
* [133:0] Zoned voice commands -- IE saying "turn on the lights" and only the lights in your current room or zone turn on.

## Adaptability

* [71:0] Tighter integration with my schedule. Automating actions that I regularly ask it to do such as turning off lights at a certain time because that is what I usually ask for.
* [82:0] Learning preferences over time
* [114:1] Integration from other services to anticipate what I need/want
* [143:0] Wenn man nach Hause kommt, soll Google den Nutzer anhand der Begrüßung erkennen und dessen präferierten Einstellungen  (Lampenschaltung, Musik einschalten, Raumtemperatur einstellen etc.) einschalten.

## Customization

* [1:0] Mehr Konfigurationsmöglichkeiten was sprachbefehle anbelangt.
* [12:2] Personalisiertes Antwortverhalten der Sprachsteuerung. Manche Benutzer möchten/brauchen vielleicht mehr Nachfragen/Bestätigungen/Feedback vom System und manche sind zufrieden, wenn die gewollte Aktion ausgeführt wird.
* [17:0] More user flexibility (allow wide range of custom tasks, change trigger word, response phrases, etc)
* [50:0] When device doesn't know what I asked for to give the option to design an action.
* [61:1] Better/customization feedback loop.E.g. I do not want feedback for all requests.
* [78:0] more speaker voice options
* [78:1] more reliable wakeup phrases or renaming of device to respond to
* [78:2] more voice options, i would pay a reasonable fee for a voice that sounds the same as stephen hawkings computer voice module, or the hal9000
* [83:0] Ability to change to custom wake words
* [92:1] Easier means of developing "personal" skills not intended to be published.
* [103:0] Better customizability by consumers (power users mainly)
* [114:0] Trigger phrase personalization
* [121:2] Customizable queue phrases.
* [142:0] Ability to control feedback easier sometimes the voice is to loud for the rest of the home when late/early
* [143:2] "Ok Google, ich gehe jetzt" --> Google schaltet alle Lampen und evtl. Musik etc aus.
* [143:3] "Ok Google, guten morgen" --> Google schaltet schon mal den Wasserkocher oder die Kaffeemaschine an.

# Feedback

* [12:2] Personalisiertes Antwortverhalten der Sprachsteuerung. Manche Benutzer möchten/brauchen vielleicht mehr Nachfragen/Bestätigungen/Feedback vom System und manche sind zufrieden, wenn die gewollte Aktion ausgeführt wird.
* [61:1] Better/customization feedback loop.E.g. I do not want feedback for all requests.
* [83:1] Less verbose responses
* [88:0] feinstgliederiges Feedback: Nutzer weiss dann immer voran sie sind, ob zugehört wird, wieviel verstanden wurde
* [108:1] Shorter pauses between responses, sometimes there are awkward faps in the assistants speech when responding to a question.
* [142:0] Ability to control feedback easier sometimes the voice is to loud for the rest of the home when late/early

# Capability Communication

* [1:1] Ein Dokument mit Befehlen hätte ich gerne.  Bei google home kommt man nur mit Ausprobieren an die Lösung. Dabei weiß ich gar nicht was alles möglich ist. Auch wüsste ich gerne, ob es möglich ist Musik von einem Raum in den anderen "mitzunehmen". Aber ich weiß nicht wie der Befehl sein könnte, und zum Googlen fehlt mir der Begriff
* [12:3] Hilfestellung zu Befehlssätzen bzw. möglichen Aktionen für unerfahrene Nutzer. Sollte automatisch an die Möglichkeiten des Smart Homes angepasst werden, um nur Befehle für vorhandene Hardware zu beschreiben.
* [54:1] Google needs to stop hiding the advanced routine/shortcut features. It can almost do what HomeKit can do, and Apple puts all their stuff upfront. It is so hidden on the Google side, that I usually just deal with the issues instead of going in to correct them.
* [55:0] Maybe teach the capabilities and applications.
* [55:1] have some kind of documentation sent out to users.
* [124:0] A big registry with a lot a voice commands
* [124:1] Keep user informed about why some functionnality was removed or doesn't work anymore

# Connectivity

* [89:1] Offline control. Allowing you to build your own smart-home without an internet connection would increase security and decrease subscription service and storage cost. Being able to update hardware via the internet is important, yes but it shouldn't be required for that device to function in a reasonable manner.
* [92:0] A "local-only" mode

# Privacy and Security

* [97:1] greater privacy
* [124:2] Offer the possibilities to lock functionnalities to specific users (do not allow buying goods to visitors for example)
* [129:1] Despite the home being a living space for multiple people, many services act as though there is only one user. Sharing credentials so that multiple people can control the house is stupid and a security risk in 2018.

# Unknown

## Unrelated to the Topic
* [19:1] I can not think of any other improvement. I can currently do pretty much everything I want to do, or at least the ability is there if you're willing to pay for it.
* [48:1] Unknown.
* [61:2] Nothing new, same as before.
* [61:3] Nothing new, same as before.
* [81:2] No additional improvements.
* [81:3] No additional improvements.

## No Matching Category
* [88:1] bestmögliche Aufmerksamkeitsverfolgung, d.h. tracking der Aufmerksamkeit des Nutzers und hierarchische Expansion von Details nur wenn Aufmerksamkeit explizit auf die Interaktion mit dem System liegt.
* [97:0] lower cost
* [117:1] Speakers integrated into ceilings, walls
* [133:1] Interoperability of the hardware products with the software

# PVA Online Survey - Results and Evaluation
This repository contains the raw data from an online survey about the usage of personal voice assistants (Alexa, Google Assistant, Siri, ...).
The study was performed as part of the master thesis 'Evaluation of the Google Assistant as a Spoken Dialogue System for the Smart Home Framework Base Cube One' (Huxohl, 2018).
In addition, the repository contains python and R scripts for evaluation of this data as part of the paper 'Interaction Guidelines for Personal Voice Assistants in Smart Homes' (Huxohl et al., 2019).

## Data
The raw data of the online survey is in the file [survey_results.csv](survey_results.csv).
The first row in the file contains the questions asked in the survey.
Each following row is made up of the answers of one participant to these questions.

## Evaluation
First, the script [extract_problems_and_improvements.py](extract_problems_and_improvements.py) extracts all the problems with - and improvements - for interactions with PVAs suggested by the participants and writes them into [problems.txt](problems.txt) and [improvements.txt](improvements.txt).
These problems and improvements were manually categorized.
The categorization is found in [categorized_problems.md](categorized_problems.md) and [categorized_improvements.md](categorized_improvements.md).

The script [evaluate_categories.py](evaluate_categories.py) evaluates this categorization by printing quantities and other information as well as creating the file [categorization.csv](categorization.csv).
The file [categorization.csv](categorization.csv) is used by the script [generate_category_plot.R](generate_category_plot.R) to create the plot [figures/categorization.pdf](figures/categorization.pdf).

The script [evaluate_demography.py](evaluate_demography.py) prints demographic information about the participants.

The script [evaluate_experience.py](evaluate_experience.py) prints as how experienced participants rated themselves with different PVAs as well as other information.
Additionally, it creates the plots [figures/experience_ratings.pdf](figures/experience_ratings.pdf) and [figures/owned_smart_speakers.pdf](figures/owned_smart_speakers.pdf).

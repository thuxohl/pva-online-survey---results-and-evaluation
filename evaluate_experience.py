#!/usr/bin/env python3

"""
Script evaluating the experience questions of the survey by
printing information about the answers
"""

import numpy as np
import matplotlib.pyplot as plt

# slightly increase font size for plots
import matplotlib
matplotlib.rcParams.update({'font.size': 6})

import util

"""
Indices for experience questions.
These were extracted by reading the output of the util script.
"""
indexFunctionPlayMusic = 18
indexFunctionControlLights = 19
indexFunctionControlRollerShutters = 20
indexFunctionControlThermostats = 21
indexFunctionSetAlarmsOrTimers = 22
indexFunctionCalendar = 23
indexFunctionRequestInformation = 24

# here the second question is used because it had to be answered
# and thus cannot contain empty strings
indexNumberSmartSpeaker = util.smartSpeakerQuestion2

indexFamiliarity = 48
indexImageLivingFulltyConnected = 49
indexIndepentendAssistant = 50

indexAlexa = 10
indexGoogleAssistant = 11
indexSiri = 12
indexCortana = 13

# create dictionaries to store values
valuesPVA = { \
        'Alexa': [], \
        'Google Assistant': [], \
        'Siri': [], \
        'Cortana': [] }
valuesSmartSpeaker = []
valuesSmartHome = { \
        'Familiarity': [], \
        'Living in a Fully Connected Home': [], \
        'Independently Acting Assistant': [] }
valuesFunctions = { \
        'Play Music': 0, \
        'Control Lights': 0, \
        'Control Roller Shutters': 0, \
        'Control Thermostats': 0, \
        'Set Alarms/Timers': 0, \
        'Calendar': 0, \
        'Information Requests': 0 }

# load results and put them into the according dictionaries
for row in util.loadSurveyResults():
    valuesPVA['Alexa'].append(util.expToNumber(row[indexAlexa]))
    valuesPVA['Google Assistant'].append(util.expToNumber(row[indexGoogleAssistant]))
    valuesPVA['Siri'].append(util.expToNumber(row[indexSiri]))
    valuesPVA['Cortana'].append(util.expToNumber(row[indexCortana]))

    valuesSmartSpeaker.append(int(float(row[indexNumberSmartSpeaker])))

    valuesSmartHome['Familiarity'].append(util.expToNumber(row[indexFamiliarity]))
    valuesSmartHome['Living in a Fully Connected Home'].append(util.expToNumber(row[indexImageLivingFulltyConnected]))
    valuesSmartHome['Independently Acting Assistant'].append(util.expToNumber(row[indexIndepentendAssistant]))

    valuesFunctions['Play Music'] += util.yesOrNoToNumber(row[indexFunctionPlayMusic])
    valuesFunctions['Control Lights'] += util.yesOrNoToNumber(row[indexFunctionControlLights])
    valuesFunctions['Control Roller Shutters'] += util.yesOrNoToNumber(row[indexFunctionControlRollerShutters])
    valuesFunctions['Control Thermostats'] += util.yesOrNoToNumber(row[indexFunctionControlThermostats])
    valuesFunctions['Set Alarms/Timers'] += util.yesOrNoToNumber(row[indexFunctionSetAlarmsOrTimers])
    valuesFunctions['Calendar'] += util.yesOrNoToNumber(row[indexFunctionCalendar])
    valuesFunctions['Information Requests'] += util.yesOrNoToNumber(row[indexFunctionRequestInformation])

# Create formatters for a nicer output
lengthFormat = '{:<' + str(len('Google Assistant')) + '}'
numberFormat = '{:.2f}'
# Print values for experience with personal voice assistants
print('Experience with PVAs:')
for key in valuesPVA:
    mean = np.mean(valuesPVA[key])
    std = np.std(valuesPVA[key])
    print(' - ' + lengthFormat.format(key), ': μ = ' + numberFormat.format(mean) + ', σ = ' + numberFormat.format(std))
print('')
# Print results for smart speakers owned
print('Smart Speakers Owned:')
print(' - μ = ' + numberFormat.format(np.mean(valuesSmartSpeaker)) + ', σ = ' + numberFormat.format(np.std(valuesSmartSpeaker)) + ', max = ' + str(max(valuesSmartSpeaker)) + ', min = ' + str(min(valuesSmartSpeaker)))
print('')
lengthFormat = '{:<' + str(len('Living in a Fully Connected Home')) + '}'
# Print results for experience with smart homes
print('Experience with Smart Homes:')
for key in valuesSmartHome:
    mean = np.mean(valuesSmartHome[key])
    std = np.std(valuesSmartHome[key])
    print(' - ' + lengthFormat.format(key), ': μ = ' + numberFormat.format(mean) + ', σ = ' + numberFormat.format(std))
print('')
lengthFormat = '{:<' + str(len('Control Roller Shutters')) + '}'
# Print results for functions used
print('Functions Used:')
for key in valuesFunctions:
    print(' - ' + lengthFormat.format(key), ': ' + str(valuesFunctions[key]))

# compute display size for tex document
textwidth = 506.295
fig_width_pt = textwidth * 0.45
inches_per_pt = 1.0 / 72.27
fig_width_in = inches_per_pt * fig_width_pt
fig_height_in = fig_width_in

# Create plot with two subplots
fig, axes = plt.subplots(2, 1, figsize=(fig_width_in, fig_height_in * 1.5))
# add plot for experience ratings from pvas
ax = axes[0]
ax.set_ylim([0.5, 5.5])
ax.set_title('a) Experience with PVAs')
ax.boxplot(valuesPVA.values(), notch=False, sym='D', showmeans=True, meanline=True, flierprops={'color': 'black'}, whis=[5,95])
ax.set_xticklabels(map(lambda key: util.insertLineBreaks(key), valuesPVA.keys()))
# uncomment for a more detailed y axis description
# ax.set_yticklabels(['', 'Not at All - 1', 'Slightly - 2', 'Moderately - 3', 'Very - 4', 'Extremely - 5'])

# add plots for experience ratings for smart homes
ax = axes[1]
ax.set_ylim([0.5, 5.5])
ax.set_title('b) Experience with Smart Homes')
ax.boxplot(valuesSmartHome.values(), notch=False, sym='D', showmeans=True, meanline=True, flierprops={'color': 'black'}, whis=[5,95])
ax.set_xticklabels(map(lambda key: util.insertLineBreaks(key), valuesSmartHome.keys()))
# uncomment for a more detailed y axis description
# ax.set_yticklabels(['', 'Not at All - 1', 'Slightly - 2', 'Moderately - 3', 'Very - 4', 'Extremely - 5'])

# adjust space to top, left, right and between subplots to use space more efficiently
plt.subplots_adjust(hspace=0.45, top=0.95, left=0.06, right=0.94)

# save figure
util.saveFigure(fig, 'experience_ratings')

# create additional histogram for the number of smart speakers owned by participants
fig_width_in = fig_width_in * 2.0
fig, ax = plt.subplots(1, 1, figsize=(fig_width_in, fig_height_in))
ax.set_title('Smart Speakers Owned per Participant')
ax.hist(valuesSmartSpeaker, bins=np.arange(max(valuesSmartSpeaker) + 2), edgecolor='black', linewidth=1.0)
ax.set_xticks(np.arange(max(valuesSmartSpeaker) + 1) + 0.5)
ax.set_xticklabels(np.arange(max(valuesSmartSpeaker) + 1))
ax.set_xlabel('Number of Smart Speakers')
ax.set_ylabel('Number of Participants')

# save figure
util.saveFigure(fig, 'owned_smart_speakers')

# uncomment to display the created plots
# plt.show()

# computa and print additional smart speaker information
at_least_one = 0
at_least_two = 0
for x in valuesSmartSpeaker: 
    if x > 0:
        at_least_one += 1
    if x > 1:
        at_least_two += 1

print('')
print('Smart Speaker Relations:')
print(' - ' + str(at_least_one) + ' participants owned at least one smart speaker')
print(' - ' + str(at_least_two) + ' participants owned at least two smart speaker')
print(' - ' + numberFormat.format(100 * at_least_two / float(at_least_one)) + '% of participants that own a smart speaker own more than one')


#!/usr/bin/env python3

"""
Script used to extract problems and improvements noted by participants.
It prints a problem (improvement) in a line to the file problems.txt (improvements.txt).
Each line encodes the id of the participants and and index indicating at which
question is was mentioned.

Additionally, it prints how many problems (improvements) were mentioned overall
and how many of them were mentioned before the suggestions by the author.
"""

import util

"""
Indices for problem an improvement questions.
These were extracted by reading the output of the util script.
"""
indicesProblems = [53, 57, 76, 80]
indicesImprovements = [61, 63, 84, 86]

# save how many problems and improvements have been mentioned
problemCount = 0
improvementCount = 0
# count how many of them were mentioned before the suggesionts by the author
problemCountBefore = 0
improvementCountBefore = 0

# open problem and improvement files
fileProblems = open('problems.txt', 'w')
fileImprovements = open('improvements.txt', 'w')
# iterate over participant data
for row in util.loadSurveyResults():
    for j in range(4):
        # extract problem
        problemText = row[indicesProblems[j]].strip()
        # filter empty problems
        if problemText != '' and problemText != '?' and problemText != 'N/A':
            # write to file and encode participant information
            fileProblems.write('[' + row[0] + ':' + str(j) + '] ' + problemText + '\n')
            # increase counter
            problemCount += 1
            if j < 2:
                problemCountBefore += 1

        # extract problem
        improvementsText = row[indicesImprovements[j]].strip()
        # filter empty problems
        if improvementsText != '' and improvementsText != '?' and improvementsText != 'N/A':
            # write to file and encode participant information
            fileImprovements.write('[' + row[0] + ':' + str(j) + '] ' + improvementsText + '\n')
            # increase counter
            improvementCount += 1
            if j < 2:
                improvementCountBefore += 1
# close files
fileProblems.close()
fileImprovements.close()

# print counts
lengthFormat = '{:<' + str(len('Improvements')) + '}'
numberFormat = '{:<' + str(len(str(problemCount))) + '}'
print(lengthFormat.format('Problems') + ':', numberFormat.format(problemCount), '- Before', problemCountBefore)
print(lengthFormat.format('Improvements') + ':', numberFormat.format(improvementCount), '- Before', improvementCountBefore)
